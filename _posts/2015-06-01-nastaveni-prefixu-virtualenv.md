---
layout: post
title: Nastaveni prefixu virtualenv a dalsi me triky
language: CS
author_name: Ondrej Sika
author_web: http://ondrejsika.com
author_email: ondrej@ondrejsika.com
author_twitter: http://twitter.com/ondrejsika
author_github: http://github.com/ondrejsika

---

Pouzivam virtualenv v kazdem projektu v adresari __.env__ to ale znamena, ze vsechny virtualni prostredi maji stejny prefix __(.env)__.

Neni to uplne prakticke, proto mam jednoduchy zpusob jak to zmenit, staci do souboru __.env/bin/avtivate__ pridat na konec tuto radku, kde __new-name__ je nazev virtualniho prostredi:

    PS1='(new-name)'$_OLD_VIRTUAL_PS1

Dalsi muj trik je zapinani virtualniho prostredi. Jelikoz mam vsechny v __.env__ muzu pouzivat alias __activate__ na aktivaci konktetniho prostredi z adresare projektu. Staci do a __.bashrc__ pridat nasledujici radku:

    alias activate='source .env/bin/activate'

To je asi vse co s virtual env delam, kazdopadne tyto 2 vychitavky mi zprijemnuji praci.

