---
layout: post
title: Example of Postgres function in Python
language: EN
author_name: Ondrej Sika
author_web: http://ondrejsika.com
author_email: ondrej@ondrejsika.com
author_twitter: http://twitter.com/ondrejsika
author_github: http://github.com/ondrejsika
---

Moved from <https://ondrejsika.com/blog/2014/04/21/example-of-postgres-function-in-python.html>


You must install PLPython extension

    apt-get install postgresql-plpython-9.3


### Example 1

    CREATE FUNCTION python_pow(x integer, n integer)
    RETURNS integer AS $$
    return x ** n
    $$ LANGUAGE 'plpythonu';

    sika=# select python_pow(3, 3);
     python
    --------
         27
    (1 row)


### Example 2

    CREATE FUNCTION python_pow(integer, integer)
    RETURNS integer AS $$
    return args[0] ** args[1]
    $$ LANGUAGE 'plpythonu';

