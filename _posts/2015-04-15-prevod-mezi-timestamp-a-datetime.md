---
layout: post
title: 'Prevod mezi timestamp a datetime'
language: CS
author_name: Ondrej Sika
author_web: http://ondrejsika.com
author_email: ondrej@ondrejsika.com
author_twitter: http://twitter.com/ondrejsika
author_github: http://github.com/ondrejsika

---

To je pomrne casta zalezitost, proto jsem si pripravil tyto 2 funkce, ktere nam to ulehci:


    import time
    import datetime

    def time2datetime(t):
        return datetime.datetime.fromtimestamp(t)

    def datetime2time(dt):
        return time.mktime(dt.timetuple())


Tyto funkce obsahuje take moje knihovan utilit [sikautils](https://github.com/ondrejsika/python-sikautils), konkretne zde <https://github.com/ondrejsika/python-sikautils/blob/master/python/timeutils.py>
