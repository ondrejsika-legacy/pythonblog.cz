---
layout: post
title: Minimalni konfigurace Python balicku
language: CS
author_name: Ondrej Sika
author_web: http://ondrejsika.com
author_email: ondrej@ondrejsika.com
author_twitter: http://twitter.com/ondrejsika
author_github: http://github.com/ondrejsika

---

Nejmensi mozna konfigurace Python balicku je pouze toto:

setup.py:

    from setuptools import setup, find_packages

    setup(
        name='pythonkniha',
        version='2',
        py_modules=['pythonkniha'],
    )

ale je to opravu minimalni, proto je podle mne lepsi pridat jeste par parametru:

setup.py:

    from setuptools import setup, find_packages

    setup(
        name='pythonkniha',
        version='2',
        author='Ondrej Sika',
        author_email='ondrej@ondrejsika.com',
        description='Example package',
        url='http://ondrejsika.com/books/python-kniha',
        py_modules=['pythonkniha'],
    )

